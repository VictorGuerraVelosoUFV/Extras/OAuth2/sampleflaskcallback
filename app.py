from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    t = ""
    for k, v in request.args.to_dict().items():
        t += f"{k}={v}\n"

    return t


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
